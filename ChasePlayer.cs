﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour {

    public CircleCollider2D cirCol;
    private bool playerDetected;
    public Transform player;
    public float speed = 3f;



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (playerDetected)
        {
            Vector3 currentPosition = transform.position;

            // Get the target waypoints position
            Vector3 targetPosition = player.transform.position;

            // If the moving object isn't that close to the waypoint
            if (Vector3.Distance(currentPosition, targetPosition) > .1f)
            {

                // Get the direction and normalize
                Vector3 directionOfTravel = targetPosition - currentPosition;
                directionOfTravel.Normalize();

                //scale the movement on each axis by the directionOfTravel vector components
                transform.Translate(
                    directionOfTravel.x * speed * Time.deltaTime,
                    directionOfTravel.y * speed * Time.deltaTime,
                    directionOfTravel.z * speed * Time.deltaTime,
                    Space.World
                );
            }
        }
		
	}


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (gameObject.name == "Player")
        {
            playerDetected = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.name == "Player")
        {
            playerDetected = false;
        }
    }


}
